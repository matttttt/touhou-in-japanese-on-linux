# Touhou IN Japanese ON Linux

Here is spiffy guide on how to get some Touhou games running in Japanese on Linux.

Wine Version: 5.18

## Touhou 6

### Problems

1. It errors out unless you run Wine with the Shift JIS locale.
1. When you run the game executable you have to be in the same directory as it or it errors out. (Maybe obvious, took me a while to understand :'( )

### Steps to Make it Work

I know this works on Arch, idk about everything else:
1. Download the contents of the th06 folder.
1. Run `add_sjis.sh` with root permissions.
1. Change the parameters of `06.sh` to work with your setup.
1. Run `06.sh`!

### Notes

- I changed the name of the game executable, because it is by default kanji which is hard to deal with on Linux.
- I wasn't able to get the install to work, so I just used the files in the `kouma` directory on the disk.

## Touhou 7

### Problems

1. Left drift.
1. When you run the game executable you have to be in the same directory as it or it errors out. (Maybe obvious, took me a while to understand :'( )

### Steps to Make it Work

1. Download the contents of the th07 folder.
1. Change the parameters of `winetricks_fixes.sh` to work with your setup.
1. Run `winetricks_fixes.sh`.
1. Change the parameters of `07.sh` to work with your setup.
1. Run `07.sh`!

### Notes

- I wasn't able to get the install to work, so I just used the files in the `youmu` directory on the disk.

## About

My brain dum, hopefully this helps someone else with dum brain disorder.
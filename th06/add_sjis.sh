#!/bin/sh

localedef -i ja_JP -f SHIFT_JIS ./ja_JP.sjis --no-warnings=ascii
sed -i '/ja_JP.UTF-8 UTF-8/a ja_JP.SJIS SHIFT_JIS ' /etc/locale.gen
locale-gen --no-warnings=ascii
